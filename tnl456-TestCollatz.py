#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    # Initial Test
    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    # Tests that it can read large numbers in
    def test_read_2(self):
        s = "999999 90\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999999)
        self.assertEqual(j, 90)
        
    # Tests that it can read the same input in
    def test_read_3(self):
        s = "10 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 10)
        
    # Tests that it can read reversed inputs
    def test_read_4(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    # Initial tests
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    # Tests that it still evaluates properly on reversed inputs
    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)
        
    # Tests that it still works when the input is the same 2 numbers
    def test_eval_6(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)
        
    # Tests that it still works when the input is the same 2 very large numbers
    def test_eval_7(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    # Tests that it still works when the input is the same 2 very small numbers
    def test_eval_8(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
        
    # Tests that it still works in the upper bounds of the range limits
    def test_eval_9(self):
        v = collatz_eval(999900, 999999)
        self.assertEqual(v, 259)
        
    # Tests that it still works when reversed in the upper bounds of the range limits
    def test_eval_10(self):
        v = collatz_eval(999999, 999900)
        self.assertEqual(v, 259)
    # -----
    # print
    # -----

    # Initial test
    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    # Tests that it still works when values given are the same
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 10, 10, 7)
        self.assertEqual(w.getvalue(), "10 10 7\n")
        
    # Tests that it still works when reversing the values
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 100, 10, 119)
        self.assertEqual(w.getvalue(), "100 10 119\n")

    # -----
    # solve
    # -----

    # Initial Test
    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # Tests over a large range
    def test_solve_2(self):
        r = StringIO("125 10000")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "125 10000 262\n")
    
    # Tests at the very edge of the range
    def test_solve_3(self):
        r = StringIO("999999 999999")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 0.076s

OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
....................
----------------------------------------------------------------------
Ran 20 tests in 0.076s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          47      1     22      2    96%   66->78, 120
TestCollatz.py      83      0      2      0   100%
------------------------------------------------------------
TOTAL              130      1     24      2    98%
"""
