#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

# hi

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "20 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  20)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "300 400\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 300)
        self.assertEqual(j, 400)

    def test_read_4(self):
        """Testing case in which i > j"""

        s = "1000 900\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1000)
        self.assertEqual(j, 900)

    def test_read_5(self):
        """Testing case in which i == j"""

        s = "10001 10001\n"
        i, j = collatz_read(s)
        self.assertEqual(i, j)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """Testing Range from 1 to 10"""

        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        """Testing Range from 1 to 10"""

        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        """Testing Range from 1 to 10"""

        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        """Testing Range from 1 to 10"""

        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        """Testing case in which i > j"""

        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        """Testing case in which i == j"""

        v = collatz_eval(10001, 10001)
        self.assertEqual(v, 180)

    def test_eval_7(self):
        """Testing very high values of i and j,
            with very large range."""

        v = collatz_eval(800000, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print_5(self):
        """Testing case in which i > j"""

        w = StringIO()
        collatz_print(w, 1000, 900, 174)
        self.assertEqual(w.getvalue(), "1000 900 174\n")

    def test_print_6(self):
        """Testing case in which i == j"""

        w = StringIO()
        collatz_print(w, 10001, 10001, 180)
        self.assertEqual(w.getvalue(), "10001 10001 180\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("4 10\n100 150\n3 6\n9 21\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4 10 20\n100 150 122\n3 6 9\n9 21 21\n")

    def test_solve_3(self):
        r = StringIO("2 4\n1000 1100\n34 48\n78 123\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 4 8\n1000 1100 169\n34 48 110\n78 123 119\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
