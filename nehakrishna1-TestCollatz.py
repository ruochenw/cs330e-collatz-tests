#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 210)
        self.assertEqual(j, None)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(-4, 10)
        self.assertEqual(v, 19)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 124)

    def test_eval_3(self):
        v = collatz_eval(210, 210)
        self.assertEqual(v, 39)

    def test_eval_4(self):
        v = collatz_eval(None, 1000)
        self.assertEqual(v, 111)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 19)
        self.assertEqual(w.getvalue(), "1 10 19\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 124)
        self.assertEqual(w.getvalue(), "100 200 124\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, None, None, 0)
        self.assertEqual(w.getvalue(), "None None 0\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 19\n100 200 124\n201 210 88\n900 1000 173\n")

    def test_solve_2(self):
        r = StringIO("-4 18\n24 39\n46 99\n69 720\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "-4 18 20\n24 39 111\n46 99 118\n69 720 170\n")

    def test_solve_3(self):
        r = StringIO("1 1\n 2 2\n 8 8\n \n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 0\n2 2 1\n8 8 3\nNone None 0\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
